import pandas as pd
from causalinference import CausalModel

df = pd.read_csv(".\output\stays.csv")
df = df[df['Diabetes Mellitus']==1]

df1 = df[['AGE', 'GENDER', 'MORTALITY','Rosiglitazone_USING', 'DOSE_VAL_RX', 'CKD']]
df2 = pd.get_dummies(df1, columns=['GENDER'], drop_first=True)

Y = df2['MORTALITY'].values
D = df2['Rosiglitazone_USING'].values
X = df2[['AGE', 'GENDER_M', 'DOSE_VAL_RX', 'CKD']].values

model = CausalModel(Y, D, X)
model.est_via_matching(bias_adj=True)

print(model.estimates)