import os
import pandas as pd
import numpy as np
import statsmodels.api as sm

input_path = '.\output'

columns = ['AGE', 'Rosiglitazone_USING', 'DOSE_VAL_RX',
       'CKD']

df = pd.read_csv(os.path.join(input_path, 'stays.csv'))
df1 = df[df['Diabetes Mellitus'] == 1]
df1['USING_Diabetes_DRUG'] = df1['USING_Diabetes_DRUG'] - df1['Rosiglitazone_USING']
y = df1['Heart Disease']
X = df1[columns]

log_reg = sm.Logit(y, X).fit(maxiter=100)
print(log_reg.summary())
