import csv
import numpy as np
import os
import pandas as pd
import sys
import argparse
import pickle as pkl

def dataframe_from_csv(path, header=0, index_col=0):
    return pd.read_csv(path, header=header, index_col=index_col)

def read_patients_table(mimic3_path):
    pats = dataframe_from_csv(os.path.join(mimic3_path, 'PATIENTS.csv'))
    pats = pats[['SUBJECT_ID', 'GENDER', 'DOB', 'DOD']]
    pats.DOB = pd.to_datetime(pats.DOB)
    pats.DOD = pd.to_datetime(pats.DOD)
    return pats

def read_admissions_table(mimic3_path):
    admits = dataframe_from_csv(os.path.join(mimic3_path, 'ADMISSIONS.csv'))
    admits = admits[['SUBJECT_ID', 'HADM_ID', 'ADMITTIME', 'DISCHTIME', 'DEATHTIME', 'ETHNICITY']]
    admits.ADMITTIME = pd.to_datetime(admits.ADMITTIME)
    admits.DISCHTIME = pd.to_datetime(admits.DISCHTIME)
    admits.DEATHTIME = pd.to_datetime(admits.DEATHTIME)
    admits = admits.loc[admits.groupby('SUBJECT_ID').ADMITTIME.idxmin()]
    return admits

def read_icd_diagnoses_table(mimic3_path):
    codes = dataframe_from_csv(os.path.join(mimic3_path, 'D_ICD_DIAGNOSES.csv'))
    codes = codes[['ICD9_CODE', 'SHORT_TITLE', 'LONG_TITLE']]
    diagnoses = dataframe_from_csv(os.path.join(mimic3_path, 'DIAGNOSES_ICD.csv'))
    diagnoses = diagnoses.merge(codes, how='inner', left_on='ICD9_CODE', right_on='ICD9_CODE')
    diagnoses[['SUBJECT_ID', 'HADM_ID', 'SEQ_NUM']] = diagnoses[['SUBJECT_ID', 'HADM_ID', 'SEQ_NUM']].astype(int)
    return diagnoses

def merge_on_subject(table1, table2):
    return table1.merge(table2, how='inner', left_on=['SUBJECT_ID'], right_on=['SUBJECT_ID'])

def add_age_to_stays(stays):
    stays['AGE'] = stays.apply(lambda e: (e['ADMITTIME'] - e['DOB']).days/365, axis=1)
    stays.ix[stays.AGE < 0, 'AGE'] = 90
    return stays

def add_inhospital_mortality_to_stays(stays):
    mortality = stays.DOD.notnull() & ((stays.ADMITTIME <= stays.DOD) & (stays.DISCHTIME >= stays.DOD))
    mortality = mortality | (stays.DEATHTIME.notnull() & ((stays.ADMITTIME <= stays.DEATHTIME) & (stays.DISCHTIME >= stays.DEATHTIME)))
    stays['MORTALITY'] = mortality.astype(int)
    #stays['MORTALITY_INHOSPITAL'] = stays['MORTALITY']
    return stays

def filter_stays_on_age(stays, min_age=18, max_age=np.inf):
    stays = stays.ix[(stays.AGE >= min_age) & (stays.AGE <= max_age)]
    return stays

def merge_on_subject_hadm(d1, d2, how='left'):
    return pd.merge(d1, d2, how=how,
                           left_on=['SUBJECT_ID', 'HADM_ID'], right_on=['SUBJECT_ID', 'HADM_ID']).fillna(0)

def filter_top_n_diagnoses(diagnoses, n):
    return diagnoses.groupby(['SUBJECT_ID', 'HADM_ID']).apply(lambda x: x.nsmallest(n,'SEQ_NUM')).reset_index(drop=True)

def filter_diagnoses_on_diseases(diagnoses):
    dis_dict = {}
    with open(".\\data\\resources\\" + 'icd_short_desc.txt') as f:
        for line in f:
            key = line[line.find(' '):].strip()
            value = line[:line.find(' ')]
            dis_dict[key] = value

    dis_map = pd.read_csv(".\\data\\resources\\disease_map.csv")
    target_dict = dict(zip(dis_map['Code'].astype(str), dis_map['Description']))
    diagnoses['CATEGORY'] = diagnoses['SHORT_TITLE'].map(dis_dict).apply(lambda x: x[:3] if x.isnumeric() else (x[:4])).map(target_dict)
    outcomes = diagnoses[['SUBJECT_ID', 'HADM_ID', 'CATEGORY']].dropna(subset=['CATEGORY'])
    outcomes['VALUE'] = 1
    patient_outcomes = outcomes.pivot_table(index=['SUBJECT_ID', 'HADM_ID'], columns='CATEGORY', values='VALUE', fill_value=0).reset_index()

    return patient_outcomes

def read_events_table(mimic3_path, filename, event_type):
    if event_type == 'labevents':
        filename = 'LABEVENTS.csv'
    
    if event_type == 'chartevents':
        filename = 'CHARTEVENTS.csv'

    event_map = pd.read_csv(".\\data\\resources\\event_map.csv")
    event_map = event_map[event_map.Type == event_type]
    event_dict = dict(zip(event_map.Code , event_map.Description))
    
    chunk_list = []
    df_chunk = pd.read_csv(os.path.join(mimic3_path, filename), chunksize=1000000)
    
    for chunk in df_chunk:
        chunk['ITEM'] = chunk.ITEMID.map(event_dict)
        chunk_filter = chunk[['SUBJECT_ID', 'HADM_ID', 'ITEM', 'VALUENUM']]
        chunk_filter = chunk.dropna(subset=['ITEM'])
        chunk_list.append(chunk_filter)

    df_concat = pd.concat(chunk_list)
    event_reading = df_concat.groupby(['SUBJECT_ID', 'HADM_ID', 'ITEM']).VALUENUM.agg(['min', 'max', 'median', 'mean']).unstack(-1)
    event_reading.columns = list(map(lambda s: '-'.join(s), list(map(lambda x: x[::-1], event_reading.columns.tolist()))))

    return event_reading

def read_prescription_table(mimic3_path, disease, medication):
    prescription = pd.read_csv(os.path.join(mimic3_path, "PRESCRIPTIONS.csv"))
    f = open(os.path.join(os.path.join(mimic3_path, "resources"), "dis_dict"), "rb")
    dis_dict = pkl.load(f)
    f.close()
    gsn = [k for k, v in dis_dict.items() if(disease in v)]
    prescription1 = prescription[prescription.GSN.isin(gsn)][['SUBJECT_ID', 'HADM_ID']].drop_duplicates()
    colname1 = 'USING_' + disease + '_DRUG'
    prescription1[colname1] = 1
    prescription2 = prescription[prescription.DRUG.str.contains("(?i)" + medication)].groupby(['SUBJECT_ID', 'HADM_ID']).first().reset_index()
    colname2 = medication + '_USING'
    prescription2[colname2] = 1
    columns = ['SUBJECT_ID', 'HADM_ID'] + [colname2] + ['DOSE_VAL_RX']
    prescription2 = prescription2[columns]

    return prescription2.merge(prescription1, how='outer', left_on=['SUBJECT_ID', 'HADM_ID'], right_on=['SUBJECT_ID', 'HADM_ID']).fillna(0)

parser = argparse.ArgumentParser(description='Extract per-subject data from MIMIC-III CSV files.')
parser.add_argument('--mimic3_path', type=str, help='Directory containing MIMIC-III CSV files.', default='.\data')
parser.add_argument('--output_path', type=str, help='Directory where per-subject data should be written.', default='.\output')
parser.add_argument('--event_tables', '-e', type=str, nargs='+', help='Tables from which to read events.', default=['LABEVENTS'])
parser.add_argument('--verbose', '-v', type=int, help='Level of verbosity in output.', default=1)

args, _ = parser.parse_known_args()

try:
    os.makedirs(args.output_path)
except:
    pass


patients = read_patients_table(args.mimic3_path)
admits = read_admissions_table(args.mimic3_path)

stays = merge_on_subject(patients, admits)

stays = add_age_to_stays(stays)
stays = add_inhospital_mortality_to_stays(stays)
stays = filter_stays_on_age(stays)
stays = stays[['SUBJECT_ID', 'HADM_ID', 'AGE', 'ETHNICITY', 'MORTALITY']]

prescription = read_prescription_table(args.mimic3_path, 'Diabetes', 'Rosiglitazone')
stays = merge_on_subject_hadm(stays, prescription)

#prescription.to_csv(os.path.join(args.output_path, 'all_prescriptions.csv'), index=False)

diagnoses = read_icd_diagnoses_table(args.mimics3_path)
diagnoses = filter_top_n_diagnoses(diagnoses, 3)
diagnoses = filter_diagnoses_on_diseases(diagnoses)
stays = merge_on_subject_hadm(stays, diagnoses)

#diagnoses.to_csv(os.path.join(args.output_path, 'all_diagnoses.csv'), index=False)

labevents = read_events_table(args.mimic3_path, 'LABEVENTS.csv', 'labevents')
stays = merge_on_subject_hadm(stays, labevents)
#labevents.to_csv(os.path.join(args.output_path, 'all_labevents.csv'), index=False)

chartevents = read_events_table(args.mimic3_path, "CHARTEVENTS.csv", 'chartevents')
stays = merge_on_subject_hadm(stays, chartevents)
stays.to_csv(os.path.join(args.output_path, 'stays.csv'), index=False)